package com.example.sheduleApp.sheduleApp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SheduleAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(SheduleAppApplication.class, args);
	}

}
